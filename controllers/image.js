const clarifai = require('clarifai');

const app = new Clarifai.App({
  apiKey: 'b2b3fe27535f4120b68f16fac401705c'
});

const handleAPICall = (req, res) => {
  app.models
    .predict(Clarifai.FACE_DETECT_MODEL, req.body.input)
    .then(data => {
      res.json(data);
    })
    .catch(() => res.status(500).json('Unable to connet to the clarifai API'));
}

const handleImage = (req, res, db) => {
  const { id } = req.body;

  return db('users').where('id', '=', id)
    .increment('entries', 1)
    .returning('entries')
    .then(entries => {
      res.json(entries[0])
    })
    .catch(() => res.status(400).json('Unable to get entries.'));

}

module.exports = {
  handleImage,
  handleAPICall
}
